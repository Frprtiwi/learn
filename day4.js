//Conditional

// let x = [8, 7, 6]
// let y = 3

// if (y < 2 && y < 5) {
//     console.log("This is first statement!");
// }
// else if (!(y < 5)) {
//     console.log("This is second statement!");
// }
// else {
//     console.log("This is third statement!");
// }

//Ganjil Genap

let number
number = [1, 2, 4, 5, 6, 9]

// console.log(number[0]);

// for (let i=0; i<10; i++){
//     console.log(i);
// }

for (let i = 0; i < number.length; i++) {
  let num = number[i]
  if (num % 2 == 0) {
    console.log(num + ' adalah bilangan genap.')
  } else {
    console.log(num + ' adalah bilangan ganjil.')
  }
}

let years
years = [2000, 2001, 2005, 2009, 2010]

for (let i = 0; i < years.length; i++) {
  let year = years[i]
  if (year % 4 == 0) {
    console.log(year + ' adalah tahun kabisat.')
  } else {
    console.log(year + ' bukan tahun kabisat.')
  }
}

let numbers
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

for (let i = 0; i < numbers.length; i++) {
  let num = numbers[i]
  if (num % 3 == 0) {
    console.log(num + ' adalah kelipatan 3.')
  } else {
    console.log(num + ' bukan kelipatan 3.')
  }
}

// *
// **
// ***
// ****
// *****
